﻿using FaceRecognitionDotNet;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using OnSive.ImageLibrary.Models;
using OnSive.ImageLibrary.Data;
using OnSive.ImageLibrary.Extentions;
using System.Diagnostics;
using Microsoft.EntityFrameworkCore;

namespace OnSive.ImageLibrary.Services;

internal static class FacialRecognitionService
{
    public static readonly Dictionary<string, List<FaceEncoding>> FaceEncodings = new();
    private static readonly FaceRecognition _faceRecognition;

    static FacialRecognitionService()
    {
        var modelParameter = new ModelParameter
        {
            PosePredictor68FaceLandmarksModel = File.ReadAllBytes(Path.Combine("TrainedModels", "shape_predictor_68_face_landmarks.dat")),
            PosePredictor5FaceLandmarksModel = File.ReadAllBytes(Path.Combine("TrainedModels", "shape_predictor_5_face_landmarks.dat")),
            FaceRecognitionModel = File.ReadAllBytes(Path.Combine("TrainedModels", "dlib_face_recognition_resnet_model_v1.dat")),
            CnnFaceDetectorModel = File.ReadAllBytes(Path.Combine("TrainedModels", "mmod_human_face_detector.dat")),
        };
        _faceRecognition = FaceRecognition.Create(modelParameter);
    }

    public static void Train()
    {
        Log.Information("Training faces from /Dataset");
        var knownEncodings = new Dictionary<string, List<double[]>>();

        var datasetDirectory = new DirectoryInfo("Dataset");
        foreach (var personDirectory in datasetDirectory.GetDirectories("*", SearchOption.TopDirectoryOnly))
        {
            var name = personDirectory.Name[(personDirectory.Name.LastIndexOf('\\') + 1)..];
            foreach (var imageFile in personDirectory.GetFiles())
            {
                try
                {
                    Log.Information($"Reading {imageFile.FullName}");
                    using var image = FaceRecognition.LoadImageFile(imageFile.FullName);
                    var locations = _faceRecognition.FaceLocations(image);
                    var encodings = _faceRecognition.FaceEncodings(image, locations);
                    foreach (var encoding in encodings)
                        if (knownEncodings.ContainsKey(name))
                        {
                            knownEncodings[name].Add(encoding.GetRawEncoding());
                            FaceEncodings[name].Add(encoding);
                        }
                        else
                        {
                            knownEncodings.Add(name, new() { encoding.GetRawEncoding() });
                            FaceEncodings.Add(name, new() { encoding });
                        }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error while reading {imageFile.FullName} - skipping...");
                }
            }
        }

        File.WriteAllText(Path.Combine("Dataset", "data.json"), JsonSerializer.Serialize(knownEncodings));
    }

    public static void Train(string personName, string imagePath, bool saveData = true)
    {
        try
        {
            Log.Information($"Training '{personName}' from '{imagePath}'");
            using var image = FaceRecognition.LoadImageFile(imagePath);
            var locations = _faceRecognition.FaceLocations(image);
            var encodings = _faceRecognition.FaceEncodings(image, locations);
            foreach (var encoding in encodings)
                if (FaceEncodings.ContainsKey(personName))
                    FaceEncodings[personName].Add(encoding);
                else
                    FaceEncodings.Add(personName, new() { encoding });

            if (saveData)
                SaveEncodings();
        }
        catch (Exception ex)
        {
            Log.Error(ex, $"Error while reading {imagePath} - skipping...");
        }
    }

    public static void SaveEncodings()
    {
        Log.Information("Saving encodings to data.json");
        var encodings = new Dictionary<string, IEnumerable<double[]>>();
        foreach (var faceEncoding in FaceEncodings)
            encodings.Add(faceEncoding.Key, faceEncoding.Value.Select(x => x.GetRawEncoding()));
        File.WriteAllText(Path.Combine("Dataset", "data.json"), JsonSerializer.Serialize(encodings));
    }

    internal static void LoadEncodings()
    {
        Log.Information("Loading face encodings from data.json");
        var data = File.ReadAllText(Path.Combine("Dataset", "data.json"));
        var encodings = JsonSerializer.Deserialize<Dictionary<string, List<double[]>>>(data);
        if (encodings != null)
            foreach (var encoding in encodings)
            {
                FaceEncodings.Add(encoding.Key, new());
                foreach (var raw in encoding.Value)
                {
                    FaceEncodings[encoding.Key].Add(FaceRecognition.LoadFaceEncoding(raw));
                }
            }
    }

    internal static ImageEntity? CheckImage(string imagePath)
    {
        var sw = new Stopwatch();
        sw.Start();
        try
        {
            using var context = new DataContext();
            Log.Information($"Checking image '{imagePath}'");

            var md5 = Helper.CalculateMD5(imagePath);
            var imageEntity = context.Images.FirstOrDefault(x => x.MD5 == md5);
            if (imageEntity == null)
            {
                Log.Information($"Image is new and is registered");
                imageEntity = new()
                {
                    MD5 = md5,
                    FacesInPicture = new List<FaceEntity>(),
                };
                context.Add(imageEntity);
            }
            else
            {
                Log.Information($"Found image in database");
                if (imageEntity.HasNoFaces)
                {
                    Log.Information($"Image does not contain any faces");
                    return null;
                }

                context.Entry(imageEntity).Collection(x => x.FacesInPicture).Load();
            }

            if (imageEntity.FacesInPicture.Count == 0 || imageEntity.FacesInPicture.Any(x => x.PersonId == null))
            {
                Log.Information($"There are still untagged faces, starting scan");
                FindFaces(context, imagePath, imageEntity);
            }
            else
            {
                Log.Information($"All faces are already tagged, skipping scan");
                foreach (var face in imageEntity.FacesInPicture)
                    context.Entry(face).Reference(x => x.Person).Load();
            }

            context.SaveChanges();
            return imageEntity;
        }
        catch (Exception ex)
        {
            Log.Error(ex, "An error occured!");
            return null;
        }
        finally
        {
            sw.Stop();
            Log.Information($"Needed {sw.Elapsed} to finish image check");
        }

    }

    private static void FindFaces(DataContext context, string imagePath, ImageEntity imageEntity)
    {
        using var img = FaceRecognition.LoadImageFile(imagePath);
        var locations = _faceRecognition.FaceLocations(img);
        if (!locations.Any())
        {
            Log.Information($"Image does not contain any faces");
            imageEntity.HasNoFaces = true;
            imageEntity.FacesInPicture = new List<FaceEntity>();
            return;
        }

        var locationFaces = new Dictionary<Location, FaceEntity>();
        foreach (var location in locations)
        {
            var faceInPicture = imageEntity.FacesInPicture.GetFromLocation(location);
            if (faceInPicture == null)
            {
                faceInPicture = new FaceEntity()
                {
                    Left = location.Left,
                    Top = location.Top,
                    Right = location.Right,
                    Bottom = location.Bottom,
                };
                imageEntity.FacesInPicture.Add(faceInPicture);
            }
            else if (faceInPicture.PersonId != null)
            {
                context.Entry(faceInPicture).Reference(x => x.Person).Load();
            }

            locationFaces.Add(location, faceInPicture);
        }

        if (locationFaces.Any(x => x.Value.PersonId == null))
        {
            Log.Information("Found previously unknown faces, starting recognition");
            var encodings = _faceRecognition.FaceEncodings(img, locations).ToArray();
            const double tolerance = 0.5d;
            for (int i = 0; i < encodings.Length; i++)
            {
                var location = locations.ElementAt(i);
                if (locationFaces[location].PersonId != null)
                {
                    Log.Information($"Skipping already known face '{locationFaces[location].Person!.Name}' at index {i}");
                    continue;
                }

                foreach (var item in FaceEncodings)
                {
                    foreach (var knownEncoding in item.Value)
                    {
                        var match = FaceRecognition.CompareFace(knownEncoding, encodings[i], tolerance);
                        if (match)
                        {
                            TagFace(context, locationFaces[location], item.Key);
                            break;
                        }
                    }
                }
            }
        }
    }

    internal static void TagFace(DataContext context, FaceEntity face, string name)
    {
        var personEntity = context.Persons.FirstOrDefault(x => x.Name == name);
        if (personEntity == null)
        {
            Log.Information($"Recognized new person '{name}'");
            personEntity = new()
            {
                Name = name,
            };
            context.Add(personEntity);
        }
        else
        {
            Log.Information($"Recognized '{name}'");
        }
        face.Person = personEntity;
        context.Entry(face).State = EntityState.Modified;
    }
}