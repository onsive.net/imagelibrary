﻿using OnSive.ImageLibrary.Classes;
using OnSive.ImageLibrary.Data;
using OnSive.ImageLibrary.Models;
using OnSive.ImageLibrary.Services;
using OnSive.ImageLibrary.Views;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace OnSive.ImageLibrary.Controls;

public partial class FoundFace : UserControl
{
    internal readonly FaceEntity FaceEntity;
    private readonly Bitmap _image;

    public FoundFace(FaceEntity faceEntity, Bitmap image)
    {
        FaceEntity = faceEntity;
        _image = image;
        InitializeComponent();
    }

    private void UserControl_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
        var offsetX = 100;
        var offsetY = 200;

        var left = FaceEntity.Left - offsetX;
        var top = FaceEntity.Top - offsetY;
        var width = FaceEntity.Right - FaceEntity.Left + offsetX * 2;
        var height = FaceEntity.Bottom - FaceEntity.Top + offsetY * 2;

        if (left < 0)
            left = 0;
        if (top < 0)
            top = 0;
        if (left + width > _image.Width)
            width = _image.Width - left;
        if (top + height > _image.Height)
            height = _image.Height - height;

        var rect = new Rectangle(left, top, width, height);
        var image = _image.Clone(
            rect,
            PixelFormat.DontCare);

        var dialog = new TextDialog("Please enter the persons name:", FaceEntity.Person?.Name ?? "")
        {
            Owner = Application.Current.MainWindow
        };
        dialog.ShowDialog();
        var name = dialog.Text;

        var fileInfo = new FileInfo(Path.Combine("Dataset", name, Guid.NewGuid().ToString() + ".png"));
        if (!fileInfo.Directory!.Exists)
            fileInfo.Directory.Create();
        image.Save(fileInfo.FullName, ImageFormat.Png);

        using (var context = new DataContext())
        {
            FacialRecognitionService.Train(name, fileInfo.FullName);
            FacialRecognitionService.TagFace(context, FaceEntity, name);
            context.SaveChanges();
        }
        Update(System.Windows.Media.Brushes.AliceBlue, name);
    }

    public void Update(System.Windows.Media.Brush brush)
    {
        this.BorderBrush = brush;
    }

    public void Update(System.Windows.Media.Brush brush, string name)
    {
        this.BorderBrush = brush;
        this.label.Content = name;
    }
}