﻿using Microsoft.EntityFrameworkCore;
using OnSive.ImageLibrary.Data;
using OnSive.ImageLibrary.Services;
using Serilog;
using System.IO;
using System.Threading.Tasks;
using System.Windows;

namespace OnSive.ImageLibrary;

public partial class App : Application
{
    public delegate void LoadingHandler();
    public static event LoadingHandler? OnLoadingFinished;

    private void Application_Startup(object sender, StartupEventArgs e)
    {
        Helper.ShowConsole();

        Log.Logger = new LoggerConfiguration()
            .WriteTo.Console()
            .CreateLogger();

        MainWindow = new Views.MainWindow();
        MainWindow.Show();

        Task.Run(() =>
        {
            using (var context = new DataContext())
            {
                Log.Debug("Migrating database...");
                context.Database.Migrate();
            }

            if (!Directory.Exists("Dataset"))
                Directory.CreateDirectory("Dataset");

            if (!File.Exists(Path.Combine("Dataset", "data.json")))
                FacialRecognitionService.Train();
            else
                FacialRecognitionService.LoadEncodings();
            OnLoadingFinished?.Invoke();
        });
    }
}