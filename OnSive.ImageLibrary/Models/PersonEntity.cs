﻿using System.Collections.Generic;

namespace OnSive.ImageLibrary.Models;

public class PersonEntity
{
    public ulong Id { get; set; }
    public string Name { get; set; }
    public virtual ICollection<FaceEntity> PicturesOfPerson { get; set; }
}