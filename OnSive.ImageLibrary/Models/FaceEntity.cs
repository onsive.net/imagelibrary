﻿namespace OnSive.ImageLibrary.Models;

public class FaceEntity
{
    public ulong Id { get; set; }
    public ulong ImageId { get; set; }
    public virtual ImageEntity Image { get; set; }
    public ulong? PersonId { get; set; }
    public virtual PersonEntity? Person { get; set; }
    public int Left { get; set; }
    public int Top { get; set; }
    public int Right { get; set; }
    public int Bottom { get; set; }
}