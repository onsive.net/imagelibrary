﻿using System.Collections.Generic;

namespace OnSive.ImageLibrary.Models;

public class ImageEntity
{
    public ulong Id { get; set; }
    public string MD5 { get; set; }
    public bool HasNoFaces { get; set; }
    public virtual ICollection<FaceEntity> FacesInPicture { get; set; }
}