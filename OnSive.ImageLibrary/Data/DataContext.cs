﻿using Microsoft.EntityFrameworkCore;
using OnSive.ImageLibrary.Models;

namespace OnSive.ImageLibrary.Data;

internal class DataContext : DbContext
{
    public DbSet<PersonEntity> Persons { get; set; }
    public DbSet<ImageEntity> Images { get; set; }
    public DbSet<FaceEntity> Faces { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite(@"DataSource=data.db;");
        base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
    }
}