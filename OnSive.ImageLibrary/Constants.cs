﻿namespace OnSive.ImageLibrary;

internal static class Constants
{
    public const string AppName = "[OS] Facial Recognition";
    public const string AppVersion = "1.0.0-alpha+1";
}