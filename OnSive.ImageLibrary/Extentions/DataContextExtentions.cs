﻿using FaceRecognitionDotNet;
using Microsoft.EntityFrameworkCore;
using OnSive.ImageLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OnSive.ImageLibrary.Extentions;

internal static class DataContextExtentions
{
    /// <summary>
    /// Rollt alle EF-Veränderungen zurück bei einem Fehler in der <paramref name="function"/>-Funktion.
    /// </summary>
    internal static void UseTransaction<TDbContext>(this TDbContext dbContext, Action<TDbContext> function)
        where TDbContext : DbContext
    {
        using var transaction = dbContext.Database.BeginTransaction();
        function(dbContext);
        transaction.Commit();
    }

    /// <summary>
    /// Rollt alle EF-Veränderungen zurück bei einem Fehler in der <paramref name="function"/>-Funktion.
    /// </summary>
    internal static TValue UseTransaction<TDbContext, TValue>(this TDbContext dbContext, Func<TDbContext, TValue> function)
        where TDbContext : DbContext
    {
        using var transaction = dbContext.Database.BeginTransaction();
        var result = function(dbContext);
        transaction.Commit();
        return result;
    }

    /// <summary>
    /// Rollt alle EF-Veränderungen zurück bei einem Fehler in der <paramref name="function"/>-Funktion.
    /// </summary>
    internal static async Task UseTransactionAsync<TDbContext>(this TDbContext dbContext, Func<TDbContext, CancellationToken, Task> function, CancellationToken cancellationToken = default)
        where TDbContext : DbContext
    {
        await using var transaction = await dbContext.Database.BeginTransactionAsync(cancellationToken).ConfigureAwait(false);
        await function(dbContext, cancellationToken);
        await transaction.CommitAsync(cancellationToken).ConfigureAwait(false);
    }

    /// <summary>
    /// Rollt alle EF-Veränderungen zurück bei einem Fehler in der <paramref name="function"/>-Funktion.
    /// </summary>
    internal static async Task<TValue> UseTransactionAsync<TDbContext, TValue>(this TDbContext dbContext, Func<TDbContext, CancellationToken, Task<TValue>> function, CancellationToken cancellationToken = default)
        where TDbContext : DbContext
    {
        await using var transaction = await dbContext.Database.BeginTransactionAsync(cancellationToken).ConfigureAwait(false);
        var result = await function(dbContext, cancellationToken);
        await transaction.CommitAsync(cancellationToken).ConfigureAwait(false);
        return result;
    }

    internal static FaceEntity? GetFaceEntity(this DbSet<FaceEntity> dbSet, Location location, int offset = 50)
    {
        var returnValue = dbSet.Where(x => x.Left - offset > location.Left && x.Left + offset < location.Left && x.Top - offset > location.Top && x.Top + offset < location.Top);
        if (returnValue.Count() > 1)
            returnValue = returnValue.Where(x => x.Right - offset > location.Right && x.Right + offset < location.Right && x.Bottom - offset > location.Bottom && x.Bottom + offset < location.Bottom);
        return returnValue.FirstOrDefault();
    }

    internal static FaceEntity? GetFromLocation(this ICollection<FaceEntity> dbSet, Location location, int offset = 5)
    {
        var returnValue = dbSet.Where(x => x.Left - offset < location.Left && x.Left + offset > location.Left && x.Top - offset < location.Top && x.Top + offset > location.Top);
        if (returnValue.Count() > 1)
            returnValue = returnValue.Where(x => x.Right - offset < location.Right && x.Right + offset > location.Right && x.Bottom - offset < location.Bottom && x.Bottom + offset > location.Bottom);
        return returnValue.FirstOrDefault();
    }
}