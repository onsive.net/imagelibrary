﻿using OnSive.ImageLibrary.Models;
using System;

namespace OnSive.ImageLibrary.Classes;

internal class RecognitionEventArgs : EventArgs
{
    public ImageEntity? ImageEntity { get; }

    internal RecognitionEventArgs(ImageEntity? imageEntity)
    {
        ImageEntity = imageEntity;
    }
}