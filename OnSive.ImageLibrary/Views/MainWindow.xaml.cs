﻿using OnSive.ImageLibrary.Controls;
using OnSive.ImageLibrary.Models;
using OnSive.ImageLibrary.Services;
using Serilog;
using System;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MBrush = System.Windows.Media.Brush;
using MBrushes = System.Windows.Media.Brushes;

namespace OnSive.ImageLibrary.Views;

public partial class MainWindow : Window
{
    private Bitmap _image;
    private int _borderBrushesIndex = 0;
    private static readonly MBrush[] _borderBrushes = new[]
    {
        MBrushes.Green,
        MBrushes.Blue,
        MBrushes.Orange,
        MBrushes.Turquoise,
        MBrushes.Pink,
    };

    public MainWindow()
    {
        InitializeComponent();

        this.Title = $"{Constants.AppName} [{Constants.AppVersion}]";

        App.OnLoadingFinished += App_OnLoadingFinished;
    }

    private void App_OnLoadingFinished()
    {
        this.Dispatcher.Invoke(() => loadingGrid.Visibility = Visibility.Collapsed);
    }

    private async void Window_Drop(object sender, DragEventArgs e)
    {
        try
        {
            string filePath;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                filePath = files[0];
            }
            else
                return;

            this.Dispatcher.Invoke(() =>
            {
                ResetImageControl();

                _image = new(filePath);
                RotateImageAutomatically(_image);
                imageControl.Source = Helper.ImageSourceFromBitmap(_image);
                loadingGrid.Visibility = Visibility.Visible;
                dropGrid.Visibility = Visibility.Collapsed;
            });

            var imageEntity = await Task.Run(() => FacialRecognitionService.CheckImage(filePath));
            if (imageEntity != null)
                this.Dispatcher.Invoke(() => DrawFaceRectangles(imageEntity));
        }
        catch (Exception ex)
        {
            Log.Error(ex, "Exception while recognizing faces");
        }
        finally
        {
            this.Dispatcher.Invoke(() =>
            {
                loadingGrid.Visibility = Visibility.Collapsed;
            });
        }
    }

    private void ResetImageControl()
    {
        var controlls = imageGrid.Children.OfType<FoundFace>().ToArray();
        for (int i = 0; i < controlls.Length; i++)
            imageGrid.Children.Remove(controlls[i]);
        imageControl.Source = null;
    }

    private void DrawFaceRectangles(ImageEntity imageEntity)
    {
        foreach (var face in imageEntity.FacesInPicture)
        {
            var control = new FoundFace(face, _image);
            if (face.Person != null)
            {
                control.label.Content = face.Person.Name;
                control.BorderBrush = _borderBrushes[_borderBrushesIndex++];
                if (_borderBrushesIndex == _borderBrushes.Length)
                    _borderBrushesIndex = 0;
            }
            else
            {
                control.label.Content = "Unknown";
            }
            control.Margin = GetScaledThickness(face);
            imageGrid.Children.Add(control);
        }
    }

    private void Window_DragEnter(object sender, DragEventArgs e)
    {
        dropGrid.Visibility = Visibility.Visible;
    }

    private void Window_DragLeave(object sender, DragEventArgs e)
    {
        dropGrid.Visibility = Visibility.Collapsed;
    }

    private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
    {
        foreach (var control in imageGrid.Children.OfType<FoundFace>())
            control.Margin = GetScaledThickness(control.FaceEntity);
    }

    private Thickness GetScaledThickness(FaceEntity faceEntity)
    {
        var offsetX = (imageGrid.ActualWidth - imageControl.ActualWidth) / 2;
        var offsetY = (imageGrid.ActualHeight - imageControl.ActualHeight) / 2;

        var scaleX = imageControl.ActualWidth / _image.Width;
        var scaleY = imageControl.ActualHeight / _image.Height;

        var left = faceEntity.Left * scaleX + offsetX;
        var top = faceEntity.Top * scaleY + offsetY;
        var right = imageGrid.ActualWidth - faceEntity.Right * scaleX - offsetX;
        var bottom = imageGrid.ActualHeight - faceEntity.Bottom * scaleY - offsetY;

        return new Thickness(left, top, right, bottom);
    }

    private static void RotateImageAutomatically(Bitmap bmp)
    {
        var pi = bmp.PropertyItems.Select(x => x).FirstOrDefault(x => x.Id == 0x0112);
        if (pi == null) return;

        byte o = pi.Value[0];

        if (o == 2) bmp.RotateFlip(RotateFlipType.RotateNoneFlipX);
        if (o == 3) bmp.RotateFlip(RotateFlipType.RotateNoneFlipXY);
        if (o == 4) bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
        if (o == 5) bmp.RotateFlip(RotateFlipType.Rotate90FlipX);
        if (o == 6) bmp.RotateFlip(RotateFlipType.Rotate90FlipNone);
        if (o == 7) bmp.RotateFlip(RotateFlipType.Rotate90FlipY);
        if (o == 8) bmp.RotateFlip(RotateFlipType.Rotate90FlipXY);
    }
}