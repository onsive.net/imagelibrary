﻿using System.Windows;
using System.Windows.Input;

namespace OnSive.ImageLibrary.Views;

public partial class TextDialog : Window
{
    public string Text { get; private set; }

    public TextDialog(string labelText, string defaultText = "")
    {
        InitializeComponent();
        label.Content = labelText;
        textBox.Text = defaultText;
        Text = defaultText;
        textBox.Focus();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
        Text = textBox.Text;
        this.Close();
    }

    private void TextBox_PreviewKeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
            Button_Click(sender, e);
    }
}